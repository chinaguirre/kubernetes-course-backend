package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController implements UsersApi {

  private final UsersService service;

  @Autowired
  public UsersController(UsersService service) {
    this.service = service;
  }

  @Override
  public int retrievePointsByUserId(String userId) {
    return service.retrievePoints(userId);
  }
}
