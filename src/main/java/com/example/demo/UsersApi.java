package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;

public interface UsersApi {

  @GetMapping(value = "/users/{userId}/points")
  @ResponseStatus(HttpStatus.OK)
  int retrievePointsByUserId(@PathVariable String userId);
}
